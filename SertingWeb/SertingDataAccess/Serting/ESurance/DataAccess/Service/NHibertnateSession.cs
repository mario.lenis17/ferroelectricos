﻿using NHibernate;
using NHibernate.Cfg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Serting.ESurance.DataAccess.Service
{
    public class NHibertnateSession
    {
        public static ISession OpenSession()
        {
            try
            {
                var configuration = new Configuration();
                configuration.Configure();
                configuration.AddAssembly("SertingInfrastructure");
                ISessionFactory sessionFactory = configuration.BuildSessionFactory();
                return sessionFactory.OpenSession();
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
